"""
dupre: duplicate rate estimation of sequencing libraries from small subsamples
"""

import sys
from os.path import basename, splitext
from argparse import ArgumentParser
from .occupancy import upsample, downsample, statistics, objectcount, PULP_AVAILABLE


def get_argument_parser():
    p = ArgumentParser(
        description = "dupre: duplicate rate estimation from small subsamples",
        epilog = "In development. Use at your own Risk."
        )
    p.add_argument("--observed", "-o", nargs='+', metavar='OCCUPANCY',
        help="observed occupancy vector (space-separated ints, or a filename)")
    p.add_argument("--target", "-N", metavar='TARGET', 
        help="target size, relative (ends with x) or absolute (integer), e.g. '5x' or '1000000')")
    p.add_argument("--truth", "--full", "-a", nargs='+', metavar='OCCUPANCY',
        help="true occupancy vector of the full dataset (space-separated ints, or a filename)")
    p.add_argument("--subsample", "-n", metavar='SUBSAMPLE',
        help="subsample size, relative (ends with x) or absolute (integer), e.g. '0.01x' or '10000'")
    p.add_argument("--verbose", "-v", action="store_true",
        help="verbose output")
    p.add_argument("--complexity", "-c", action="store_true",
        help="output complexity instead of duplication rate")
    p.add_argument("--K0", "-K", type=int, metavar='INT', default=25,
        help="occupancy number above which to use the heuristic [25]")
    p.add_argument("--zwidth", "-z", type=float, metavar="STDDEVs", default=2.0,
        help="allowed standard deviation for each expected occupancy")
    p.add_argument("--Zwidth", "-Z", type=float, metavar="STDDEVs", default=1.0, 
        help="allowed standard deviation of sum of most significant terms")
    p.add_argument("--name", metavar="NAME",
        help="name of this problem instance")
    p.add_argument("--histogram", "-H", action="store_true",
        help="instance data is given as PRESEQ histogram file(s)")
#    p.add_argument("--tolerance", "-t", metavar="FLOAT", type=float, default=0.01,
#        help="objective function tolerance when assessing range of duplicate rate")
    p.add_argument("--stripnames", action="store_true",
        help="strip instance names of observed occupancy vector of last component for lookup")
    p.add_argument('--version', action='version', version='%(prog)s 0.1')
    return p


def occtype(occ):
    """
    Determine whether a filename or a list of floats/ints is given as occupancy vector.
    Return 'ints' or 'file'.
    """
    occtype = 'ints'
    if len(occ) == 1:
        try:
            occ = float(occ[0])
        except ValueError:
            occtype = 'file'
    return occtype


def determine_filetype(fnames, name, hist):
    """
    1. make sure that fnames is a list containing exactly one file name
    2. determine whether the file is in histogram format or instance format
    3. check whether determined format agrees with given command line option hist
    Return (filetype, filename, instancename), where filetype is either
    'histogram', where the given name is retured (or part of the filename if None), or
    'instances', where the given name MUST be None.
    """
    assert len(fnames) == 1
    fname = fnames[0]
    line = ""
    lookslikehist = False
    with open(fname, "rt") as f:
        for line in f:
            line = line.strip()
            if line.startswith("#"):  continue
            fields = line.split()
            if len(fields) == 2:
                if fields[0].isdigit() and fields[1].isdigit():
                    lookslikehist = True
            break
    if hist and lookslikehist:
        if name is None:
            # determine name from fname, this is a quick hack.
            name = splitext(basename(fname))[0].partition("_")[0]
        return ("histogram", fname, name)
    if (not hist) and (not lookslikehist):
        if name is not None:
            raise RuntimeError("cannot use --name with instance files; names must be in file")
        return ("instances", fname, None)
    raise RuntimeError("file format / --histogram option mismatch")        


def read_instances(f):
    """
    yield (name, occupancy_vector) for each instance, where
    - name is a string (that may contain spaces),
      the last word in name is not a valid number.
    - occupancy_vector is a Python list of ints
    """
    for line in f:
        line = line.strip()
        if line.startswith("#"):  continue
        fields = line.split()
        if len(fields) == 0:  continue
        # determine which fields belong to name: at least the first one,
        # or those that do not start with a digit
        namefields = [i for (i,f) in enumerate(fields) if not f.isdigit()]
        ##print(namefields, file=sys.stderr)
        nameidx = 1 + (max(namefields)  if len(namefields)>0  else 0)
        name = " ".join(fields[:nameidx])
        vector = list(map(int, fields[nameidx:]))
        yield name, vector


def read_histogram(f):
    """
    return an occupancy vector (as a Python list) from a histogram file, such as
    1 243534
    2 85653
    3 2345
    5 2
    First column must be strictly increasing integers.
    Second column must be arbitrary integers/floats.
    """
    hist = [(0,0)]
    lastidx = 0
    for line in f:
        line = line.strip()
        if line.startswith("#"):  continue
        fields = line.split()
        if len(fields) != 2:
            raise RuntimeError("line with too few / too many fields: '{}'".format(line))
        idx = int(fields[0])
        if idx <= lastidx:
            raise RuntimeError("histogram indices not increasing")
        lastidx = idx
        hist.append( (idx, int(fields[1])) )
    occlen = 1 + max(idx for (idx,_) in hist)
    occ = [0] * occlen
    for (idx, val) in hist:
        occ[idx] = val
    return occ[1:]


def compute_target(n, targetspec):
    if targetspec.endswith(("X", "x")):
        # relative size
        factor = float(targetspec[:-1])
        return factor*n
    return float(targetspec)

    
def solve_instance(observed, args, truth=None):
    # from observed n, compute target size
    n = objectcount(observed)
    target = compute_target(n, args.target)
    result = upsample(observed, target, args.K0, z=args.zwidth, Z=args.Zwidth)
    # if truth is known, add it to result
    if truth is not None:
        (_, cxtrue, _, drtrue) = statistics(truth)
        result.update(drtrue=drtrue, cxtrue=cxtrue, atrue=truth)
    # print result to stdout, warnings to stderr
    if args.name is not None:
        result.name = args.name
    if args.verbose:
        print("[{}]".format(result.name if result.name is not None else "Result"))
        d = result.__dict__
        for key in sorted(d.keys()):
            print(key, "=", d[key])
        print()
    else:
        if args.complexity:
            print(result.oneline_complexity())
        else:
            print(result.oneline_dr())
    if not (result.status==1 and result.statusmin==1 and result.statusmax==1):
        print("# warning: status != 1: status={} statusmin={} statusmax={}".format(
            result.status, result.statusmin, result.statusmax))


def main_solve(args):
    # we have args.observed and args.target (absolute or relative)
    if occtype(args.observed) == 'ints': 
        observed = list(map(int, args.observed))
        solve_instance(observed, args)
        return
    # we are dealing with a file of observations (all with the same target)
    ft, fname, name = determine_filetype(args.observed, args.name, args.histogram)
    if ft == "instances":
        with open(fname) as fobs:
            for (name, observed) in read_instances(fobs):
                args.name = name
                solve_instance(observed, args)
    elif ft == "histogram":
        args.name = name
        with open(fname) as fobs:
            observed = read_histogram(fobs)
            solve_instance(observed, args)
    else:
        raise RuntimeError("unknown instance file type")


def evaluate_instance_1(truth, args):
    # from true N, compute subsample size
    N = objectcount(truth)
    n = compute_target(N, args.subsample)
    observed = downsample(truth, n/N)
    args.target = str(N)
    solve_instance(observed, args, truth=truth)
 
def main_eval1(args):
    # we have args.truth and args.subsample (absolute or relative)
    if occtype(args.truth) == 'ints':
        truth = list(map(int, args.truth))
        evaluate_instance_1(truth, args)
        return
    # we are dealing with a file of true occupancy vectors
    ft, fname, name = determine_filetype(args.truth, args.name, args.histogram)
    if ft == "instances":
        with open(fname) as ftruth:
            for (name, truth) in read_instances(ftruth):
                args.name = name
                evaluate_instance_1(truth, args)
    elif ft == "histogram":
        args.name = name
        with open(fname) as ftruth:
            truth = read_histogram(ftruth)
            evaluate_instance_1(truth, args)
    else:
        raise RuntimeError("unknown instance file type")


def get_truth_vector(truths, key, stripnames=True):
    if key in truths: 
        return truths[key]
    if not stripnames: 
        return None
    nn = key
    while len(nn) > 0:
        nn = nn.rpartition(" ")[0]
        if nn in truths:
            return truths[nn]
    return None


def evaluate_instance_2(truth, observed, args):
    N = objectcount(truth)
    args.target = str(N)
    solve_instance(observed, args, truth=truth)


def main_eval2(args):
    # we have args.truth and args.observed
    ttruth = occtype(args.truth)
    tobs = occtype(args.observed)
    if ttruth == 'ints' and tobs == 'ints':
        truth = list(map(int, args.truth))
        observed = list(map(int,args.observed))
        evaluate_instance_2(truth, observed, args)
        return
    if ttruth == 'file' and tobs == 'ints':
        observed = list(map(int,args.truth))
        ft, fname, name = determine_filetype(args.truth, args.name, args.histogram)
        if ft == "instances":
            with open(fname) as ftruth:
                for (name, truth) in read_instances(ftruth):
                    args.name = name
                    evaluate_instance_2(truth, observed, args)
        elif ft == "histogram":
            args.name = name
            with open(fname) as ftruth:
                truth = read_histogram(ftruth)
            evaluate_instance_2(truth, observed, args)
        else:
            raise RuntimeError("unknown instance file type")
        return        
    if ttruth == 'ints' and tobs == 'file':
        truth = list(map(int, args.truth))
        ft, fname, name = determine_filetype(args.observed, args.name, args.histogram)
        if ft == "instances":
            with open(fname) as fobs:
                for (name, observed) in read_instances(fobs):
                    args.name = name
                    evaluate_instance_2(truth, observed, args)
        elif ft == "histogram":
            args.name = name
            with open(fname) as fobs:
                observed = read_histogram(fobs)
            evaluate_instance_2(truth, observed, args)
        else:
            raise RuntimeError("unknown instance file type")
        return
    if ttruth == 'file' and tobs == 'file':
        ftt, fnamet, namet = determine_filetype(args.truth, args.name, args.histogram)
        fto, fnameo, nameo = determine_filetype(args.observed, args.name, args.histogram)
        if ftt == "instances" and fto == "instances":
            with open(fnamet) as ft:
                truths = {namet: truth  for (namet,truth) in read_instances(ft)}
            with open(fnameo) as fo:
                for (nameo, observed) in read_instances(fo):
                    truth = get_truth_vector(truths, nameo, args.stripnames)
                    if truth is None:
                        raise ValueError("observed name not among true names: '{}'".format(nameo))
                    args.name = nameo
                    evaluate_instance_2(truth, observed, args)
            return
        if ftt == "histogram" and fto == "histogram":
            with open(fnameo) as fobs:
                observed = read_histogram(fobs)
            with open(fnamet) as ftruth:
                truth = read_histogram(ftruth)
            args.name = nameo
            evaluate_instance_2(truth, observed, args)
            return
        raise RuntimeError("inconsistent file types")
    raise RuntimeError("occupancy vector types are neither proper int nor proper file")
    

def main(cargs=None):
    if not PULP_AVAILABLE:
        raise RuntimeError("Error:  pulp not available. Install with 'pip install pulp'.")
    p = get_argument_parser()
    args = p.parse_args() if cargs is None else p.parse_args(cargs)
    # determine program mode
    usagehint = " use either -o with -N (observed subsample with target size; real problem solving),\n" +\
        "or -a with -n (known truth with random downsampling; evaluation),\n" +\
        "or -a with -o (observed subsample with known truth; evaluation),\n"
    if (args.observed is not None) and (args.target is not None):
        # solve mode
        if (args.truth is not None) or (args.subsample is not None):
            raise RuntimeError(usagehint)
        mode = 'solve'
    elif (args.truth is not None) and (args.subsample is not None):
        # evaluation mode 1
        if (args.observed is not None) or (args.target is not None):
            raise RuntimeError(usagehint)
        mode = 'eval1'
    elif (args.truth is not None) and (args.observed is not None):
        # evaluation mode 2
        if (args.target is not None) or (args.subsample is not None):
            raise RuntimeError(usagehint)
        mode = 'eval2'
    else:
        raise RuntimeError(usagehint)
    # run appropriate function
    func = dict(solve=main_solve, eval1=main_eval1, eval2=main_eval2)[mode]
    func(args)
        

if __name__ == "__main__":
    main()

# This is a possible true occupancy vector:
# 282618 194892 226082 110045 167508 188650 27137 40106 26220 60895 92327 78940 48081 20759 49393 31228
# This is a subsample:
# 316323 57259 8712 1054 98 12

# dupre -a 2500000 2083295 147568 112343 107782 4254 3227 2657 1421 1257 451 385 309 135 69 64 36 36 34 3 -o 81216 445 4 --name "monotone i50 r1" --K0 20
# output: monotone i50 r1   0.2409   0.1293 0.5298   0.3953
#
