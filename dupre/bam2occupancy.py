import sys
from itertools import islice
import argparse

import numpy as np
import h5py
import pysam

from .occupancy import occupancy_array_from_values


def get_argument_parser():
    p = argparse.ArgumentParser(
        description = "bam2occupancy: obtain occupancy vectors from BAM files",
        epilog = "In development. Use at your own Risk."
        )
    p.add_argument('input', metavar="PATH",
        help="input .bam file, or  hash .h5 file created by a previous run")
    p.add_argument("--name", "-n",
        help="name for this occupancy vector (prepended to occupancy vector)")
    p.add_argument('--hashfile', '-H', metavar="H5PATH",
        help="for .bam input, write sorted hash file to the given path")
    p.add_argument('--verbose', '-v', action="store_true",
        help="for .bam input, output progress messages every million reads to stderr")
    return p

    
def hashes(f, verbose=False):
    for i, read in enumerate(f):
        if verbose and i % 1000000 == 0:
            print("# bam2occupancy: {} reads processed".format(i), file=sys.stderr)
        if not read.is_read1:  continue
        if read.is_unmapped:  continue
        yield hash((read.tid, read.pos, read.mpos))


def main(cargs=None):
    p = get_argument_parser()
    args = p.parse_args() if cargs is None else p.parse_args(cargs)
    filename = args.input

    if filename.endswith('.bam'):
        # open bam file and process reads
        with pysam.Samfile(filename, "rb") as f:
            memo = np.fromiter(hashes(f, args.verbose), dtype=np.int64)
        if args.verbose:
            print("# bam2occupancy: {} fragments hashed".format(len(memo)), file=sys.stderr)
        np.sort(memo)
    elif filename.endswith(('.h5', '.hdf5')):
        # opens a hashes file and load the hashes
        with h5py.File(filename, "r") as f:
            memo = f['hashes'][:]
    else:
        raise RuntimeError("bam2occupancy: unknown file type (must be .bam or .h5 hash file)")

    if args.name:
        print(args.name, *occupancy_array_from_values(memo))
    else:
        print(*occupancy_array_from_values(memo))

    # save the read hashes as HDF5 file if desired
    if args.hashfile:
        with h5py.File(args.hashfile, 'w') as h5f:
            h5f.create_dataset('hashes', data=memo)


if __name__ == '__main__':
    main()

