# compute and cluster hypergeometric distributions

import numpy as np
from scipy.stats import hypergeom


def hypergeometric_matrix(N, n, maxc=-1, maxk=-1):
    """
    return (0..maxc) x (0..maxk) matrix P such that
    P[c,k] = Prob[k copies in n-subsample | c copies in N-dataset].
    P[c,k] is the k-th element of the (N,c,n) hypergeometric distribution.
    """
    if maxc < 0:  maxc = N
    if maxk < 0:  maxk = n
    P = np.zeros((maxc+1, maxk+1), dtype=np.double)
    P[0,0] = 1.0
    for c in range(1,maxc+1):
        p_c = hypergeom(N,c,n)
        P[c,0:maxk+1] = p_c.pmf(np.arange(maxk+1))
    return P

def distribution_distances(N, n, maxk=30, maxc=-1):
    r = n/N
    if maxc == -1:
        maxc = int(round(2.0 * maxk / r))
    P = hypergeometric_matrix(N, n, maxc=maxc, maxk=maxk)
    D = np.zeros((maxc+1, maxc+1), dtype=np.double)  # distance matrix
    for c in range(maxc+1):
        Pc = P[c,:]
        for c2 in range(c, maxc+1):
            D[c,c2] = D[c2,c] = N * distance_l1(Pc, P[c2,:])
    return D


def distance_l1(x, y):
    m = len(x)
    s = 0.0
    for i in range(m):
        s += abs(x[i]-y[i])
    return s
