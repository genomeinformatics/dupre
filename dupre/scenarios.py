import sys
import random
from random import randint
from pandas import DataFrame

from .occupancy import statistics, strip


def scenario_monotone(first=2500000):
    """
    Return an occupancy vector a with monotone values.
    The first value is given, each subsequent value is uniformly
    distributed between 0 and the current one.
    """
    a = []
    v = first
    while v != 0:
        a.append(v)
        v = randint(0,v)
    strip(a)
    return a


def scenario_uniform(base=10000000//8, length=16):
    """
    Return an occupancy vector a of the given length
    with random uniform values between 0 and the given base.
    The provided default parameters generate vectors a with
    len=20, sum=10 mio, readcount=XXX TODO.
    """
    a = [randint(0, base//(c+1)) for c in range(length)]
    strip(a)
    return a
 
    
def _scenario_exponential(base=100000, rate=0.9):
    """
    return occupancy vector a with value bounds decreasing exponentially
    from the given base with the given rate.
    The actual values are uniform between zero and the bound,
    i.e., may be non-montonous.
    The provided default parameters generate vectors a with
    len=100, sum=1 mio, readcount=10 mio.
    """
    a = []
    while base != 0:
        a.append(randint(base//2,2*base))
        base = int(base*rate)
    strip(a)
    return a

    
def scenario_hard():
    """hard case: long occupancy vector, long tail"""
    return _scenario_exponential(80000, 0.9)


def scenario_easy():
    """easy case: short occupancy vector, sharp drop, monotone"""
    return _scenario_exponential(7000000, 0.1)


def scenario_dirac(c=10, N=10000000):
    """
    return an occupancy vector with only the c-th component being
    nonzero for a total of approximately N reads.
    """
    a = [0] * c
    a[-1] = N//c
    strip(a)
    return a


### methods for computing and printing scenario properties    

def averages(scenario, T=1000):
    """
    compute statistical averages of a scenario
    by averaging length, sum, readcount and duplication rate 
    of the occupancy vector over T random instances.
    """
    labels = ["length", "sum", "reads", "duprate"]
    df = DataFrame(columns=labels, index=np.arange(T))
    for t in range(T):
        a = scenario()
        df.loc[t] = statistics(a)
    return df.mean()


def print_scenarios_properties(scenarios, file=sys.stdout):
    for s in scenarios:
        print(">>>",s.__name__, file=file)
        print(averages(s), file=file)
