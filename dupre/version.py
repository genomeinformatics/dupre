__version__ = "0.1"

DESCRIPTION = """
dupre is a tool for duplicate rate estimation
in sequenced DNA libraries by subsampling a small fraction
of the sequencing/mapping information.

In comparison to exact determination,
the dupre approach allows to quickly estimate 
the true duplicate rate of the library
from a small sequenced subsample
before (expensively) sequencing the whole library.

"""


