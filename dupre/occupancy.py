import sys
from itertools import count
from collections import Counter
from random import sample
from math import log2, ceil, sqrt
import numpy as np
from scipy.stats import hypergeom

try:
    import pulp
    from pulp import LpVariable
    PULP_AVAILABLE = True
except ImportError:
    PULP_AVAILABLE = False
    

def objectcount(occ):
    """
    return read count of occupancy vector occ.
    occ[0] is the occupancy number of families of size 1;
    in general occ[k] is the occupancy number of families of size (k+1).
    """
    return sum((i+1)*o for i, o in enumerate(occ))

  
def statistics(occ):
    """
    return (length, sum_or_complexity, objectcount, duplicaterate) of occupancy vector occ.
    """
    s = sum(occ)
    N = objectcount(occ)
    dr = 1.0 - s/N  if N > 0  else 0.0
    return (len(occ), s, N, dr)

    
def variance(occ, q):
    """
    return complexity variance estimator for given full occupancy vector and q=1-n/N
    """
    return sum(oc * q**(c+1) * 1-q**(c+1) for c, oc in enumerate(occ))


def strip(occ, epsilon=0.0):
    """
    remove trailing zeros (or small elements) from occupancy vector occ
    (returns None, modifies occ).
    """
    last = len(occ)-1
    while last > 0 and occ[last] <= epsilon:  last -= 1
    occ[last+1:] = []

def stripped(occ, epsilon=0):
    """
    return copy of occ with trailing zeros (or small elements) removed
    """
    last = len(occ) - 1
    while last > 0 and occ[last] <= epsilon:  last -= 1
    return occ[:last+1]


def zinterval(obs, z=2.0):
    """
    Let X have a Poisson(mu) approx Normal(mu, mu) distribution.
    Let x be an observation from X.
    Find those mu such that x is in the z-interval around mu;
    that is, let [L,U] = {mu : x in [mu - z*sqrt(mu), mu + z*sqrt(mu)]}.
    
    Given a list/array obs of observations x and scalar z >= 0.0,
    return corresponding arrays L, U with lower/upper interval bounds.
    """
    # The following code solves x in [mu-a,mu+a] <=>  mu in [x-b,x+b]
    # by (in this case) solving a quadratic equation,
    # assuming that a is proportional to sqrt(mu).
    y = np.array(obs, dtype=np.double)
    s = z * np.sqrt(y+z**2 / 4.0)
    b = y + z**2 / 2.0
    xmin = b - s
    xmax = b + s
    return xmin, xmax


def default_weights(observed):
    return [1.0/sqrt(oo+1.0) for oo in observed]
    
    
def error(observed, estimated, weights=None):
    """return weighted absolute difference between observed and estimated"""
    # ensure observed and estimated have same length, pad with zeros -> x,y
    est, obs = list(estimated), list(observed)
    if len(estimated) <= len(observed):
        est = est + [0.0]*(len(observed)-len(estimated))
    else:
        obs = obs + [0.0]*(len(estimated)-len(observed))
    if weights is None:
        weights = default_weights(obs) #  [1.0/sqrt(o+1) for o in obs]
    assert len(obs) == len(est)
    # compute distance measure between x and y
    err = sum(w*abs(xx-yy) for w,xx,yy in zip(weights,est,obs))
    return err


def value_list_from_occupancy(occ):
    """return a list of values whose occupancy vector is the given list"""
    # pre-allocate list
    N = objectcount(occ)
    L = [0] * N
    # treat unique objects first (with c=1 copy), their number is occ[0]
    L[0:occ[0]] = range(1,occ[0]+1)
    # Now treat objects with c+1 copies for c >= 1
    rid = occ[0]+1  # next object ID
    i = occ[0]      # next position in L to write to
    for c in range(1, len(occ)):
        for r in range(occ[c]):
            # create a read with c+1 copies
            L[i:i+c+1] = [rid] * (c+1)
            rid += 1
            i += c+1
    return L

def value_array_from_occupancy(occ):
    """return an array of values whose occupancy vector is the given array"""
    # pre-allocate numpy array
    N = objectcount(occ)
    L = np.zeros((N,), dtype=np.uint64)
    # treat unique objects first (with c=1 copy), their number is occ[0]
    L[0:occ[0]] = np.arange(1, occ[0]+1, dtype=np.uint64)
    # Now treat reads with c+1 copies for c >= 1
    rid = occ[0]+1  # next read ID
    i = occ[0]      # next position in L to write to
    for c in range(1, len(occ)):
        ones = np.ones((c+1,), dtype=np.uint64)
        for r in range(occ[c]):
            # create a read with c+1 copies
            L[i:i+c+1] = rid * ones
            rid += 1
            i += c+1
    return L

    
def occupancy_list_from_values(values):
    """
    return the occupancy list occ for the given value list,
    such that occ[c] is the number of families with c+1 copies.
    """
    # count each value and then count occurrences of each counter
    C = Counter(values)
    occupancy = Counter(C.values())
    ks = list(occupancy.keys())
    lastk = max(ks) if len(ks) > 0 else 0
    o = [occupancy[k] for k in range(1,lastk+1)]
    return o

def occupancy_array_from_values(values):
    """
    return the occupancy array occ for the given value array,
    such that occ[c] is the number of families with c+1 copies.
    """
    n = len(values)
    new_objects_at = np.nonzero(np.diff(np.sort(values)))[0]
    first = new_objects_at[0] + 1
    last = n-1 - new_objects_at[-1]
    minlength = 1 + max(first,last)
    occ = np.bincount(np.diff(new_objects_at), minlength=minlength)
    occ[first] += 1
    occ[last] += 1
    assert occ[0] == 0
    return occ[1:]


def downsamples_pylist(a, rho=0.1, nsamples=1):
    """
    Yield 'nsamples' occupancy vectors (as Python lists)
    after downsampling given occupancy vector 'a' by a factor of 'rho'.
    
    If the given 'a' is empty, has zero objects, or the sample has zero objects,
    nothing is yielded and the generator exits immediately.
    
    Downsampling is explicit, do not run on vectors with 
    object counts exceeding a few million!
    This implementation uses Python lists and Counters.
    """
    if len(a)==0:  return
    N = objectcount(a)  # size of full sample
    n = round(N*rho)    # size of subsample
    if N==0 or n==0:  return
    L = value_list_from_occupancy(a)
    for t in range(nsamples):
        yield occupancy_list_from_values(sample(L,n))

def downsamples_numpy(a, rho=0.1, nsamples=1):
    """
    Yield 'nsamples' occupancy vectors (as Python lists)
    after downsampling given occupancy vector 'a' by a factor of 'rho'.
    
    If the given 'a' is empty, has zero objects, or the sample has zero objects,
    nothing is yielded and the generator exits immediately.
 
    This implementation uses numpy arrays instead of Python lists or Counter objects.
    Nevertheless, the yielded objects are Python lists.
    """
    if len(a)==0:  return
    N = objectcount(a)  # size of full sample
    n = round(N*rho)    # size of subsample
    if N==0 or n==0:  return
    L = value_array_from_occupancy(a)
    for t in range(nsamples):
        np.random.shuffle(L)  # can re-shuffle for each new sample
        yield list(occupancy_array_from_values(L[:n]))

downsamples = downsamples_numpy

def downsample(a, rho=0.1):
    try:
        return next(downsamples(a, rho=rho, nsamples=1))
    except StopIteration:
        return []


def hypergeometric_matrix(N, n, maxc=-1, maxk=-1):
    """
    return (0..maxc) x (0..maxk) matrix P such that
    P[c,k] = Prob[k copies in n-subsample | c copies in N-dataset].
    P[c,k] is the k-th element of the (N,c,n) hypergeometric distribution.
    """
    if maxc < 0:  maxc = N
    if maxk < 0:  maxk = n
    P = np.zeros((maxc+1, maxk+1), dtype=np.double)
    P[0,0] = 1.0
    for c in range(1,maxc+1):
        p_c = hypergeom(N,c,n)
        P[c,0:maxk+1] = p_c.pmf(np.arange(maxk+1))
    return P


def compute_heuristic_C_N(observed, K0, n, N):
    """heuristically upsample components from observed beyond K0"""
    cutobs = observed[K0:]
    complexity = sum(cutobs)
    cutn = sum((K0+1+i)*oo for (i,oo) in enumerate(cutobs))
    cutN = round(N/n * cutn)
    return (complexity, cutN)

    
class Instance:
    def __init__(self, **kwargs):
        self.__dict__.update(kwargs)
    def update(self, **kwargs):
        self.__dict__.update(kwargs)


class Result:
    """representation of an upsampling result"""
    def __init__(self, **kwargs):
        self.complexity = None
        self.dr = None
        self.drtrue = None
        self.complexity = None
        self.cxtrue = None
        self.name = None
        self.__dict__.update(kwargs)
        
    def __str__(self):
        return str(self.__dict__)
        
    def update(self, **kwargs):
        self.__dict__.update(kwargs)
    
    def oneline_dr(self, name=None):
        if hasattr(self, 'name'):
            name = self.name
        if name is None:
            name = "dr___drlower_drupper" if self.drtrue is None  else "dr___drlower_drupper___drtrue"
        if self.drtrue is None:
            return "{}   {:.4f}   {:.4f} {:.4f}".format(name, self.dr, self.drlower, self.drupper)
        else:
            return "{}   {:.4f}   {:.4f} {:.4f}   {:.4f}".format(name, self.dr, self.drlower, self.drupper, self.drtrue)

    def oneline_complexity(self, name=None):
        if hasattr(self, 'name'):
            name = self.name
        if name is None:
            name = "cx___cxlower_cxupper" if self.cxtrue is None  else "cx___cxlower_cxupper___cxtrue"
        if self.cxtrue is None:
            return "{}   {:.0f}   {:.0f} {:.0f}".format(
                name, self.complexity, self.cxlower, self.cxupper)
        else:
            return "{}   {:.0f}   {:.0f} {:.0f}   {:.0f}".format(
                name, self.complexity, self.cxlower, self.cxupper, self.cxtrue)


# This is the main function implementing our method.
def upsample(observed, N, K0=None, C=None, z=2.0, Z=2.0):
    """
    Given an 'observed' occupancy vector of a subsample (with n total counts),
    and a target count 'N' of the full dataset, estimate
    - the occupancy vector a of the full dataset (N counts)
    - its complexity Gamma (number of original reads), and
    - its duplicate rate d = (N - Gamma) / N
    as well as a 'z'*sigma confidence interval [dmin, dmax].
    
    Return a Result object with attributes
    - dr: estimated duplicate rate
    - drlower, drupper: bounds of z*sigma confidence interval for dr
    - status, statusmin, statusmax: LP solver status, should all == 1
    
    Parameters:
        observed: Python list of observed occupancies in the subsample
        N: total read count of the full dataset
    Options:
        K0: If len(observed)>K0, cut off high occupancy values 
            and treat them separately with a heuristic to reduce problem size.
            If not given, solve full problem (slow for long 'observed' vectors)
        C:  maximum occupancy number in full dataset;
            if not given, it is estimated based on the observed occupancies.
    """
    if type(observed) != list:
        raise TypeError("'observed' must be a Python list")
    (klast, s, n, _) = statistics(observed)
    if N < n:
        raise ValueError("must have N >= n for upsampling")
    q = 1.0 - n/N  # complement of subsampling rate
    ##print(observed, n, N, klast)
    # determine whether the heuristic must be used.
    if K0 is None:  K0 = klast
    if klast > K0:
        heuristic = True
        Ch, Nh = compute_heuristic_C_N(observed, K0, n, N)
        observed = observed[:K0]
        (klast, s, n, _) = statistics(observed)
        N = N - Nh
    else:
        heuristic = False
        Ch, Nh = 0, 0
    # compute LP dimensions
    assert klast <= K0
    C, K = estimate_dimensions(N, n, klast, C)
    ##print(C,K)
    # set up LP data and call point LP builder/solve
    P = hypergeometric_matrix(N,n,C,K)
    observed = observed + [0]*(K-klast)
    lp, lpresult = _build_and_solve_point_lp(observed, N, n, C, K, P)
    # adjust lpresult for heuristic
    lpresult.heuristic_used = heuristic
    lpresult.complexity += Ch
    lpresult.N = N + Nh
    lpresult.dr = (lpresult.N - lpresult.complexity) / lpresult.N
    # modify LP (and re-solve) to obtain range
    ##bound = lpresult.objective * (1.0 + tolerance)
    ##print("objective function was:", lpresult.objective, lpresult.delta)
    ##print("new bound is:", bound)
    minresult, maxresult = _build_and_solve_range_lp(lp, z, Z)
    lp = None;  del lp
    # adjust results for heuristic
    minresult.complexity += Ch
    minresult.N = N + Nh
    minresult.dr = (minresult.N - minresult.complexity) / minresult.N  # upper dr
    if minresult.dr < lpresult.dr:  minresult.dr = lpresult.dr
    maxresult.complexity += Ch
    maxresult.N = N + Nh
    maxresult.dr = (maxresult.N - maxresult.complexity) / maxresult.N  # lower dr
    if maxresult.dr > lpresult.dr:  maxresult.dr = lpresult.dr
    lpresult.update(cxlower=minresult.complexity, cxupper=maxresult.complexity)
    lpresult.update(drlower=maxresult.dr, drupper=minresult.dr)
    lpresult.update(statusmin=minresult.status, statusmax=maxresult.status)
    return lpresult


def estimate_dimensions(N, n, Klast, C=None, t=0.8, tau=0.01):
    """
    Estimate problem dimensions based on N, n
    and maximal index Klast of an observed nonzero entry o_k.
    (Note: you may want to cut off the observation beforehand,
    in order to obtain a smaller Klast.)
    
    Return estimated dimensions (C,K).

    >>> estimate_dimensions(100*10**6, 10**6, 20, t=0.8, tau=0.01)
    (1710, 27)
    >>> estimate_dimensions(100*10**6, 10**6, 20, t=0.05, tau=0.0001)
    (2902, 51)
    
    If not given, C is chosen such that fraction t (e.g., t=0.2: 20%)
    of the probability mass of P_C would end up to the left of (incl.) Klast.
    In other words, if any type-C families existed, 
    a fraction of 1-t (eg., 80%) of them would produce observed subtypes
    beyond Klast, but we have no observations there.
    Smaller values of t are "safer" and more conservative, but slower.
    Note: It is probably safe to use t=0.8 (conservative: 0.05).

    Given C, we find the K such that almost all of its probability mass
    (fraction 1-tau) is contained in 0..K.
    It is probably safe to use tau=0.01 (conservative: 0.0001).
    """
    if C is None:
        hg = hypergeom
        for c in count(1):
            if hg(N,c,n).cdf(Klast) <= t:
                C = c
                break
    p_c = hypergeom(N,C,n)
    TAU = 1 - tau
    K = max(Klast+5, int(p_c.ppf(TAU)))
    return C, K

    
def _build_and_solve_point_lp(observed, N, n, C, K, P, weights=None):
    # Initialize the LP problem and define variables
    problem = pulp.LpProblem("Occupancy from subsamples", pulp.LpMinimize)
    CONT = pulp.LpContinuous
    # solution variables a, where a[c] = number of read types with c+1 copies
    a = [ LpVariable("a{}".format(c+1), 0.0, N/(c+1), CONT) 
            for c in range(C) ]
    # expected occupancies in the subsample for a given candidate vector a
    x = [ LpVariable("x{}".format(k+1), 0.0, n/(k+1), CONT)
          for k in range(K)]
    # absolute differences between observed and x: d[k] = |observed[k]-x[k]|
    d = [ LpVariable("d{}".format(k+1), 0.0, None, CONT)
          for k in range(K)]
          
    # connect d[k] to |observed[k]-x[k]|
    for xx, oo, dd in zip(x, observed, d):
        problem +=   xx - oo <= dd
        problem +=  -xx + oo <= dd
        
    # connect x to the candidate solution a
    for k in range(K):
        problem +=  x[k] == sum(a[c] * P[c+1,k+1] for c in range(k, C))

    # the occupancy variable for unseen families
    x_0 = LpVariable("x0", 0.0, N, CONT) # TODO lower the upper bound?

    # the x_0 candidate
    problem += x_0 == sum(a[c] * P[c+1,0] for c in range(0, C))

    # the species count including unseen species is equal in observed and full
    problem += x_0 == sum(a) - sum(observed)

    # the total number of reads in the full dataset must be (close to) N
    problem +=  sum((c+1) * aa for c, aa in enumerate(a)) == N

    # define the objective function to be minimized:
    # the (weighted) sum of |observed[k]-x[k]| over all k
    if weights is None:
        weights = default_weights(observed)
    assert len(weights) == len(observed)
    problem += sum( [ww*dd for (ww, dd) in zip(weights, d)] )

    # store some interesting problem data in the Result
    instance = Instance(problem=problem, a=a, x=x, delta=d, observed=observed, weights=weights, N=N, n=n)
    lpresult = Result(N_in=N, n_in=n, C=C, K=K, weights=weights)
    
    # solve the problem and remember the values in a, x and d.
    status = problem.solve(pulp.solvers.PULP_CBC_CMD(
        options=['-primalT 1E-10', '-dualT 1E-10'], msg=0))
    objvalue = pulp.value(problem.objective)
    alist = [aa.varValue for aa in a]
    (_, complexity, N_out, dr) = statistics(alist)
    zs=[ww*dd.varValue for ww,dd in zip(weights,d)]    
    lpresult.update(status=status, a=alist, x=[xx.varValue for xx in x],
        dr=dr, N_out=N_out, complexity=complexity, zs=zs,
        delta=[dd.varValue for dd in d], objective=objvalue,
        )
    instance.update(zs=zs)
    return instance, lpresult


def _build_and_solve_range_lp(instance, z, Z):
    problem = instance.problem
    a = instance.a

    # bound weighted deltas' sum and individual values
    relevant_deltas = [i for (i,oo) in enumerate(instance.observed) if oo >= 25]
    len_delta = 1 + (max(relevant_deltas)  if len(relevant_deltas) > 0  else 0)
    Zsum = 0.0
    for (i, ww, dd, zz) in zip(count(), instance.weights, instance.delta, instance.zs):
        zzz = max(zz*1.01, z)
        problem +=  (ww*dd <= zzz)
        if i < len_delta:
            Zsum += max(zz*1.01, Z)
    problem +=  (sum(ww*dd for (ww, dd) in zip(instance.weights, instance.delta[:len_delta])) <= Zsum)

    # set objective to minimize complexity
    problem += sum(a)
    status = problem.solve(pulp.solvers.PULP_CBC_CMD(
        options=['-primalT 1E-10', '-dualT 1E-10'], msg=0))
    amin = [aa.varValue for aa in a]
    complexity = pulp.value(problem.objective)  if status==1  else float('Inf')
    minresult = Result(status=status, complexity=complexity, a=amin)

    # set objective to maximize complexity
    problem += -sum(a)
    status = problem.solve(pulp.solvers.PULP_CBC_CMD(
        options=['-primalT 1E-10', '-dualT 1E-10'], msg=0))
    amax = [aa.varValue for aa in a]
    complexity = -pulp.value(problem.objective)  if status==1  else float('Inf')
    maxresult = Result(status=status, complexity=complexity, a=amax)
    
    return minresult, maxresult

    
def _build_and_solve_equivalence_variance_lp(instance, bound, q):
    problem = instance.problem
    a = instance.a    

    # bound delta by known optimal solution
    problem += sum( [ww*dd for (ww, dd) in zip(instance.weights, instance.delta)] ) <= bound

    # set objective to maximize complexity (minimize duplicate rate)
    problem += -sum(a)
    # solve the problem and remember the values in a, x and d.
    status = problem.solve(pulp.solvers.PULP_CBC_CMD(
        options=['-primalT 1E-10', '-dualT 1E-10'], msg=0))
    amax = [aa.varValue for aa in a]
    (_, complexity, _, dr) = statistics(amax)
    maxresult = Result(status=status, complexity=complexity, dr=dr, a=amax)
    
    # set objective to minimize complexity (maximize duplicate rate)
    problem += sum(a)
    # solve the problem and remember the values in a, x and d.
    status = problem.solve(pulp.solvers.PULP_CBC_CMD(
        options=['-primalT 1E-10', '-dualT 1E-10'], msg=0))
    amin = [aa.varValue for aa in a]
    (_, complexity, _, dr) = statistics(amin)
    minresult = Result(status=status, complexity=complexity, dr=dr, a=amin)
    
    # set objective to maximize variance
    varweights = [q**c * (1 - q**c) for c in range(1,len(a)+1)]
    problem += sum(-ww * aa  for ww, aa in zip(varweights,a))
    status = problem.solve(pulp.solvers.PULP_CBC_CMD(
        options=['-primalT 1E-10', '-dualT 1E-10'], msg=0))
    variance = -pulp.value(problem.objective)  if status==1  else float('Inf')

    return maxresult, minresult, variance
