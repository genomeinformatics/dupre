# Create new python3 conda environment "dupre" with required packages

conda create --name dupre  python=3  pip numpy scipy matplotlib pandas hdf5 h5py seaborn

# To activate the environment
source activte dupre

# install additional packages (not supported by conda) with pip
pip install pulp         # for easy linear programming
pip install pysam        # for obtaining occupancy vectors from BAM files
pip install snakemake    # for running workflows

# Finally, run setup.py to install the dupre package itself.
# To do this in development mode, so repository updates become immediately active, run:
# python setup.py develop

# Otherwise run (be sure that the dupre environment is active)
python setup.py build install

# To remove:
# conda remove --name dupre --all

