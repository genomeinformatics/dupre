# coding: utf-8

import sys
from ez_setup import use_setuptools
use_setuptools()
from setuptools import setup

if sys.version_info < (3,4):
    print("At least Python 3.4 is required for dupre.", file=sys.stderr)
    exit(1)
try:
    from setuptools import setup
except ImportError:
    print("Please install setuptools before installing dupre.", file=sys.stderr)
    exit(1)

# set __version__ and DESCRIPTION
exec(open("dupre/version.py").read())

setup(
    name='dupre',
    version=__version__,
    author='Christopher Schröder and Sven Rahmann',
    author_email='christopher.schroeder@tu-dortmund.de, sven.rahmann@uni-due.de',
    description=DESCRIPTION,
    zip_safe=False,
    license='MIT',
    url='None',
    packages=['dupre'],
    entry_points={
        "console_scripts":
            ["dupre = dupre.dupre:main",
             "bam2occupancy = dupre.bam2occupancy:main",
            ]
        },
    package_data={'': ['*.css', '*.sh', '*.html']},
    classifiers=[
        "Development Status :: 5 - Production/Stable",
        "Environment :: Console",
        "Intended Audience :: Science/Research",
        "License :: OSI Approved :: MIT License",
        "Natural Language :: English",
        "Programming Language :: Python :: 3",
        "Topic :: Scientific/Engineering :: Bio-Informatics"
    ]
)

