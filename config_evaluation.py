from dupre import scenarios
from glob import glob

# use fixed random seed for reproducibility
SEED = 17

INSTANCES = 50   # instances per scenario (eg 50)
REPLICATES = 10  # replicates per instance (eg 10)
RHOS = [0.01, 0.02, 0.05, 0.10, 0.20]     # sampling rates
RATES = [round(rho*100) for rho in RHOS]  # sampling rates in percent

K0 = {1:10, 2:15, 5:20, 10:20, 20:20}  # --K0 option for dupre for each rate
assert set(K0.keys()) == set(RATES)


SCENARIO_PREFIX = 'scenario_'
ARTIFICIAL_SCENARIOS = [f[len(SCENARIO_PREFIX):] for f in scenarios.__dict__.keys() if f.startswith(SCENARIO_PREFIX)]
ARTIFICIAL_PLOT_SCENARIOS = ['easy', 'dirac', 'hard', 'monotone', 'uniform'][::-1]
assert set(ARTIFICIAL_PLOT_SCENARIOS) == set(ARTIFICIAL_SCENARIOS)

BIO_SCENARIOS = ['rna', 'exome', 'wgbs']

#challenges = glob("challenge/*_hist.txt")
#CHALLENGE_SCENARIOS = sorted(set([c.partition('challenge/')[2].rpartition('_')[0] for c in challenges]))


