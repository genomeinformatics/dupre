# dupre: Duplicate Rate Estimation #

Dupre is able to estimate the duplicate rate of a sequencing library at a given sequencing depth _N_,
when the occupancy vector of a (small) subsample is known.
This is very useful when one has to decide which sequencing depth should be aimed for,
weighing the potential of new discoveries vs. cost.

The software is written in Python 3.4 and requires additional modules.


## Installation ##

We highly recommend to use the [conda package manager](http://conda.pydata.org/miniconda.html) for Python 3.4 to install and configure the necessary Python environment.
This will allow you to keep your scientific projects independent from your system Python and install required packages in your home directory, so no root access is required.

After installing ```conda```, create a Python environment for ```dupre``` and activate it as follows:

```
#!bash
conda create --name dupre -c christopherschroeder dupre
source activate dupre
```

(On windows, the last command is just ```activate dupre```.)

If you want to run the evaluation workflow, you have to install additional packages in this environment.
```
#!bash
conda install -c bioconda snakemake seaborn
```

You should now be able to run the software; try printing the basic help information:
```
#!bash
dupre --help
```

To deactivate the Python environment, run
```
#!bash
source deactivate
```
(Again, on Windows, this is just ```deactivate```.)

To completely remove the environment, use
```
conda remove --name dupre --all
```



## Basic Usage

This section describes how to run ```dupre``` for a typical use case.

You have a list of objects and count how many times each object i appears.
Let n_i be the number of occurrences ob object i.
Next you compute the occupancy numbers o_k = {i: n_i=k}.
The vector o = (o_1, o_2, ...) is the input to dupre.

(You can use the bam2occupancy tool to get occupancy vectors from BAM files; it will tell you how many fragments appear as a copy family of size 1, 2, 3, ...).

You will also need the target number N of objects, which must be larger than the number of objects that you have (the sum of the n_i).

Assuming that o = (65610, 14580, 1620, 90, 2) and N=1000000 and you want detailed (verbose) output, you can call
```
#!bash
dupre -o 65610 14580 1620 90 2 -N 1000000 -v
```

Run 
```
#!bash
dupre --help
```
for all options.


## Contact

Please contact [Sven Rahmann](http://www.rahmannlab.de/people/rahmann)
with questions about the software,
or post bugs in the bug tracker of this repository.